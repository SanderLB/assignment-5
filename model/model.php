<?php
class Model{
	public $dom;
	public $db;
	public function __construct(){
		$this->db = new PDO('mysql:host=localhost; dbname=asg5db; charset=utf8', 'root', '');		//creates connection to sql
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$this->dom = new DOMDocument();			//creates DOM document
		$this->dom->load('Model/SkierLogs.xml');	//loads SkierLogs file
		
		$this->xpath = new DOMXPath($this->dom);	//creates xpath to run from php
	}
	
	public function parseXML(){
		
		$this->insertClub();		
		$this->insertSkier();		
		$this->insertSeason();	
		$this->insertAll();		
	}
	
	
	public function insertAll(){
		$skierName = array();		
		$seasonYear = array();		
		$clubid = array();			
		$totalDistance = array();	
		
		$names = $this->xpath->query('//SkierLogs/Season[@fallYear = "2015"]/Skiers/Skier/@userName');	//sets $names from an xpath query
        foreach($names as $t){									//sets all names in the array
            array_push($skierName, trim($t->nodeValue));		
        }
		
		$sum = 0;
		for($i = 0; $i < sizeof($skierName);$i++){		
			try{
				$temp = $this->xpath->query('//SkierLogs/Season[@fallYear="2015"]/Skiers/Skier[@userName = "'.$skierName[$i].'"]/../@clubId');					//runs xpath query
				foreach($temp as $s){							//sets all clubs in the array
					array_push($clubid, $s->nodeValue);
				}
				
				$distance = $this->xpath->query('//SkierLogs/Season[@fallYear="2015"]/Skiers/Skier[@userName="'.$skierName[$i].'"]/Log/Entry/Distance');		//runs xpath query
				for($j = 0; $j < $distance->length; $j++){
					$sum += ($distance[$j]->nodeValue);				//adds every distance for each entry that a skier skied in 2015
				}
				array_push($totalDistance, $sum);
				$sum = 0;
				
				$temp2 = $this->xpath->query('//SkierLogs/Season[@fallYear="2015"]/Skiers/Skier[@userName = "'.$skierName[$i].'"]/../../@fallYear');			//runs xpath query
				foreach($temp2 as $t){							//sets 2015 in the array
					array_push($seasonYear, $t->nodeValue);
				}
				
				$input =  $this->db->prepare("INSERT INTO skierclubseason(skierName, seasonYear, clubid, totalDistance) VALUES (:skierName, :seasonYear, :clubid, :totalDistance)");		//prepares to run mysql command
				$input->bindValue(':skierName', $skierName[$i], PDO::PARAM_STR);				//binds value
				$input->bindValue(':seasonYear', $seasonYear[$i], PDO::PARAM_STR);				//binds value
				$input->bindValue(':clubid', $clubid[$i], PDO::PARAM_STR);						//binds value
				$input->bindValue(':totalDistance', $totalDistance[$i], PDO::PARAM_STR);		//binds value
				
				$input->execute();			//runs the mysql command
				echo "Success entry for skierclubseason 2015 <br>";
			}
			catch(Eception $e){
				echo "failed to add skierclubseason 2015";
				echo $e->getMessage();
			}
		}
		
		
		$skierName16 = array();			//creates array
		$seasonYear16 = array();		//creates array
		$clubid16 = array();			//creates array
		$totalDistance16 = array();		//creates array
		
		$names16 = $this->xpath->query('//SkierLogs/Season[@fallYear = "2016"]/Skiers/Skier/@userName');			//runs xpath query
        foreach($names16 as $n16){								//sets all skier names for 2016 in the array
            array_push($skierName16, trim($n16->nodeValue));
        }
		
		$sum = 0;
		for($i = 0; $i < sizeof($skierName16);$i++){
			try{
				$temp4 = $this->xpath->query('//SkierLogs/Season[@fallYear="2016"]/Skiers/Skier[@userName = "'.$skierName16[$i].'"]/../@clubId');					//runs xpath query
				foreach($temp4 as $s16){						//sets all clubs for the skiers in 2016
					array_push($clubid16, $s16->nodeValue);
				}
				
				$distance16 = $this->xpath->query('//SkierLogs/Season[@fallYear="2016"]/Skiers/Skier[@userName="'.$skierName16[$i].'"]/Log/Entry/Distance');		//runs xpath query
				for($j = 0; $j < $distance16->length; $j++){
					$sum += ($distance16[$j]->nodeValue);			//adds every distance for each entry that a skier skied in 2016
				}
				array_push($totalDistance16, $sum);
				$sum = 0;
				
				$temp3 = $this->xpath->query('//SkierLogs/Season[@fallYear="2016"]/Skiers/Skier[@userName = "'.$skierName16[$i].'"]/../../@fallYear');				//runs xpath query
				foreach($temp3 as $t){								//adds the year 2016 to every username that has skied in the 2016 season
					array_push($seasonYear16, $t->nodeValue);
				}
				
				$input = $this->db->prepare("INSERT INTO skierclubseason(skierName, seasonYear, clubid, totalDistance) VALUES (:skierName, :seasonYear, :clubid, :totalDistance)");		//prepares to run mysql command
				$input->bindValue(':skierName', $skierName16[$i], PDO::PARAM_STR);						//binds value
				$input->bindValue(':seasonYear', $seasonYear16[$i], PDO::PARAM_STR);					//binds value
				$input->bindValue(':clubid', $clubid16[$i], PDO::PARAM_STR);							//binds value
				$input->bindValue(':totalDistance', $totalDistance16[$i], PDO::PARAM_STR);				//binds value
				
				$input->execute();							//runs mysql command
				echo "Success entry for skierclubseason 2016 <br>";
			}
			catch(Eception $e){
				echo "failed to add skierclubseason 2016";
				echo $e->getMessage();
			}
		}
		
	}
	

	public function insertSeason(){	//Adds All seasons in the database
		
		$season = array();			//creats array
		
		$seasons = $this->dom->getElementsByTagName("Season");			//gets all seasons from xml file
		
		foreach($seasons as $s){										//adds all season in the array
			array_push($season, $s->getAttribute("fallYear"));
		}
		
		for($i = 0; $i < sizeof($season);$i++){
			try{
				$input = $this->db->prepare("INSERT INTO season(fallYear) VALUES (:fallYear)");			//prepares to run mysql command
				$input->bindValue(':fallYear', $season[$i], PDO::PARAM_STR);							//binds value
				
				$input->execute();																		//runs mysql command
				echo "Success entry for season <br>";
				
			}
			catch(Eception $e){
				echo "failed entry for season";
				echo $e->getMessage();
			}
		}
	}
	
	
	
	public function insertSkier(){
		$userName = array();					//creates array
		$firstName = array();					//creates array
		$lastName = array();					//creates array
		$yearOfBirth = array();					//creates array
		
		$userNames = $this->dom->getElementsByTagName("Skier");						//gets all skier attributes
		$first = $this->dom->getElementsByTagName("FirstName");						//gets firstnames
		$last = $this->dom->getElementsByTagName("LastName");						//gets lastnames
		$birth = $this->dom->getElementsByTagName("YearOfBirth");					//gets YearOfBirth
			
		foreach($userNames as $names){						//adds all userName attributes to the array
			array_push($userName, $names->getAttribute("userName"));
		}
		foreach($first as $f){								//adds all firstnames to array
			array_push($firstName, $f->nodeValue);
		}
		foreach($last as $l){								//adds all lastnames to array
			array_push($lastName, $l->nodeValue);
		}
		foreach($birth as $b){								//adds all years of birth to array
			array_push($yearOfBirth, $b->nodeValue);
		}
		
		$userName = array_unique($userName);				//sets userName to be unique so there will be no duplicates
		
		for($i = 0; $i < sizeof($userName);$i++){
			try{
				$input = $this->db->prepare("INSERT INTO skier(userName, FirstName, LastName, YearOfBirth) VALUES (:name, :first, :last, :birth)");
				$input->bindValue(':name', $userName[$i], PDO::PARAM_STR);
				$input->bindValue(':first', $firstName[$i], PDO::PARAM_STR);
				$input->bindValue(':last', $lastName[$i], PDO::PARAM_STR);
				$input->bindValue(':birth', $yearOfBirth[$i], PDO::PARAM_STR);
				
				$input->execute();
				echo "Success entry for Skier <br>";
				
			}
			catch(Eception $e){
				echo "failed to add skier";
				echo $e->getMessage();
			}
		}
	
	
	}
	
	public function insertClub(){
		$id = array();					//creates array
		$clubName = array();			//creates array
		$city = array();				//creates array
		$county = array();				//creates array
		
		$clubs = $this->dom->getElementsByTagName("Club");			//gets all club attributes
		$names = $this->dom->getElementsByTagName("Name");			//gets nodevalue name
		$cities = $this->dom->getElementsByTagName("City");			//gets nodevalue city
		$counties = $this->dom->getElementsByTagName("County");		//gets nodevalue county
		
		foreach($clubs as $club){									//adds all attributes for Club in the array
			array_push($id, $club->getAttribute("id"));
		}
		foreach($names as $name){									//adds all club names to array
			array_push($clubName, $name->nodeValue);
		}
		foreach($cities as $citys){									//adds all club cities to array
			array_push($city, $citys->nodeValue);
		}
		foreach($counties as $c){									//adds all club county's to array
			array_push($county, $c->nodeValue);
		}
		
		for($i = 0; $i < sizeof($id);$i++){
			try{
				$input = $this->db->prepare("INSERT INTO clubs(id, clubName, city, county) VALUES (:id, :name, :city, :count)");
				$input->bindValue(':id', $id[$i], PDO::PARAM_STR);
				$input->bindValue(':name', $clubName[$i], PDO::PARAM_STR);
				$input->bindValue(':city', $city[$i], PDO::PARAM_STR);
				$input->bindValue(':count', $county[$i], PDO::PARAM_STR);
				
				$input->execute();
				echo "success entry for club <br>";
			}
			catch(Eception $e){
				echo "failed to add club";
				echo $e->getMessage();
			}
		}
	}
}



?>