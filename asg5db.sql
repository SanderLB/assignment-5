-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27. Nov, 2017 17:35 PM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asg5db`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `clubs`
--

CREATE TABLE `clubs` (
  `id` varchar(250) NOT NULL,
  `clubName` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `county` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `clubs`
--

INSERT INTO `clubs` (`id`, `clubName`, `city`, `county`) VALUES
('asker-ski', 'Asker skiklubb', 'Asker', 'Akershus'),
('lhmr-ski', 'Lillehammer Skiklub', 'Lillehammer', 'Oppland'),
('skiklubben', 'Trondhjems skiklub', 'Trondheim', 'Sør-Trøndelag'),
('vindil', 'Vind Idrettslag', 'Gjøvik', 'Oppland');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `season`
--

CREATE TABLE `season` (
  `fallYear` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `season`
--

INSERT INTO `season` (`fallYear`) VALUES
(2015),
(2016);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(250) NOT NULL,
  `firstName` varchar(250) DEFAULT NULL,
  `lastName` varchar(250) DEFAULT NULL,
  `yearOfBirth` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skier`
--

INSERT INTO `skier` (`userName`, `firstName`, `lastName`, `yearOfBirth`) VALUES
('?hal_?mos', '?Halvor', '?Mostuen', '2009'),
('?jan_tang', '?Jan', 'Tangen', '2007'),
('?rut_?mos', '?Ruth', '?Mostuen', '2002'),
('?rut_nord', '?Ruth', 'Nordli', '2006'),
('ande_andr', 'Anders', 'Andresen', '2004'),
('ande_rønn', 'Anders', 'Rønning', '2001'),
('andr_stee', 'Andreas', 'Steen', '2001'),
('anna_næss', 'Anna', 'Næss', '2005'),
('arne_anto', 'Arne', 'Antonsen', '2005'),
('arne_inge', 'Arne', 'Ingebrigtsen', '2005'),
('astr_amun', 'Astrid', 'Amundsen', '2001'),
('astr_sven', 'Astrid', 'Svendsen', '2008'),
('bent_håla', 'Bente', 'Håland', '2009'),
('bent_svee', 'Bente', 'Sveen', '2003'),
('beri_hans', 'Berit', 'Hanssen', '2003'),
('bjør_aase', 'Bjørn', 'Aasen', '2006'),
('bjør_ali', 'Bjørn', 'Ali', '2008'),
('bjør_rønn', 'Bjørg', 'Rønningen', '2009'),
('bjør_sand', 'Bjørn', 'Sandvik', '1997'),
('bror_?mos', 'Bror', '?Mostuen', '2005'),
('bror_kals', 'Bror', 'Kalstad', '2006'),
('cami_erik', 'Camilla', 'Eriksen', '2005'),
('dani_hamm', 'Daniel', 'Hammer', '2000'),
('eina_nygå', 'Einar', 'Nygård', '2009'),
('elis_ruud', 'Elisabeth', 'Ruud', '2003'),
('elle_wiik', 'Ellen', 'Wiik', '2004'),
('erik_haal', 'Erik', 'Haaland', '2007'),
('erik_lien', 'Erik', 'Lien', '2008'),
('erik_pete', 'Erik', 'Petersen', '2002'),
('espe_egel', 'Espen', 'Egeland', '2005'),
('espe_haal', 'Espen', 'Haaland', '2004'),
('eva_kvam', 'Eva', 'Kvam', '2000'),
('fred_lien', 'Fredrik', 'Lien', '2000'),
('frod_mads', 'Frode', 'Madsen', '2008'),
('frod_rønn', 'Frode', 'Rønningen', '2005'),
('geir_birk', 'Geir', 'Birkeland', '2010'),
('geir_herm', 'Geir', 'Hermansen', '2003'),
('gerd_svee', 'Gerd', 'Sveen', '2001'),
('gunn_berg', 'Gunnar', 'Berge', '2009'),
('guri_nord', 'Guri', 'Nordli', '2003'),
('hann_stei', 'Hanno', 'Steiro', '2005'),
('hans_foss', 'Hans', 'Foss', '1998'),
('hans_løke', 'Hans', 'Løken', '2005'),
('hara_bakk', 'Harald', 'Bakken', '2002'),
('heid_dani', 'Heidi', 'Danielsen', '2005'),
('helg_brei', 'Helge', 'Breivik', '2006'),
('helg_toll', 'Helge', 'Tollefsen', '2003'),
('henr_bern', 'Henrik', 'Berntsen', '2003'),
('henr_dale', 'Henrik', 'Dalen', '2005'),
('henr_lore', 'Henrik', 'Lorentzen', '2006'),
('hild_hass', 'Hilde', 'Hassan', '2007'),
('håko_jens', 'Håkon', 'Jensen', '2005'),
('idar_kals', 'Idar', 'Kalstad', '2007'),
('idar_kals1', 'Idar', 'Kalstad', '2002'),
('ida_mykl', 'Ida', 'Myklebust', '2001'),
('inge_simo', 'Inger', 'Simonsen', '2004'),
('inge_thor', 'Inger', 'Thorsen', '2006'),
('ingr_edva', 'Ingrid', 'Edvardsen', '2001'),
('juli_ande', 'Julie', 'Andersson', '2003'),
('kari_thor', 'Karin', 'Thorsen', '2002'),
('kjel_fjel', 'Kjell', 'Fjeld', '2004'),
('knut_bye', 'Knut', 'Bye', '2006'),
('kris_even', 'Kristian', 'Evensen', '2004'),
('kris_hass', 'Kristin', 'Hassan', '2003'),
('kris_hass1', 'Kristian', 'Hassan', '2004'),
('lind_lore', 'Linda', 'Lorentzen', '2004'),
('liv_khan', 'Liv', 'Khan', '2006'),
('magn_sand', 'Magnus', 'Sande', '2003'),
('mari_bye', 'Marit', 'Bye', '2003'),
('mari_dahl', 'Marit', 'Dahl', '2004'),
('mari_eile', 'Marius', 'Eilertsen', '2000'),
('mari_stra', 'Marius', 'Strand', '2005'),
('mart_halv', 'Martin', 'Halvorsen', '2002'),
('mona_lie', 'Mona', 'Lie', '2004'),
('mort_iver', 'Morten', 'Iversen', '2003'),
('nils_bakk', 'Nils', 'Bakke', '2003'),
('nils_knud', 'Nils', 'Knudsen', '2006'),
('odd_moha', 'Odd', 'Mohamed', '2005'),
('olav_bråt', 'Olav', 'Bråthen', '2000'),
('olav_eike', 'Olav', 'Eikeland', '2008'),
('olav_hell', 'Olav', 'Helle', '2007'),
('olav_lien', 'Olav', 'Lien', '2002'),
('ole_borg', 'Ole', 'Borge', '2002'),
('reid_hamr', 'Reidun', 'Hamre', '2008'),
('rolf_wiik', 'Rolf', 'Wiik', '2002'),
('rune_haga', 'Rune', 'Haga', '2005'),
('sara_okst', 'Sarah', 'Okstad', '2003'),
('silj_mykl', 'Silje', 'Myklebust', '2007'),
('sive_nord', 'Sivert', 'Nordli', '2009'),
('solv_solb', 'Solveig', 'Solbakken', '2004'),
('stia_andr', 'Stian', 'Andreassen', '2004'),
('stia_haug', 'Stian', 'Haugland', '2002'),
('stia_henr', 'Stian', 'Henriksen', '2001'),
('terj_mort', 'Terje', 'Mortensen', '2003'),
('thom_inge', 'Thomas', 'Ingebrigtsen', '2006'),
('tom_bråt', 'Tom', 'Bråthen', '2008'),
('tom_bøe', 'Tom', 'Bøe', '2008'),
('tom_jako', 'Tom', 'Jakobsen', '2002'),
('tore_gulb', 'Tore', 'Gulbrandsen', '2005'),
('tore_svee', 'Tore', 'Sveen', '2001'),
('tor_dale', 'Tor', 'Dalen', '2005'),
('tove_moe', 'Tove', 'Moe', '2002'),
('trin_kals', 'Trine', 'Kalstad', '2009'),
('tron_kris', 'Trond', 'Kristensen', '2006'),
('tron_moen', 'Trond', 'Moen', '2004'),
('øyst_aase', 'Øystein', 'Aasen', '2007'),
('øyst_lore', 'Øystein', 'Lorentzen', '2004'),
('øyst_sæth', 'Øystein', 'Sæther', '2000'),
('øyvi_hell', 'Øyvind', 'Helle', '2000'),
('øyvi_jens', 'Øyvind', 'Jenssen', '1999'),
('øyvi_kvam', 'Øyvind', 'Kvam', '2000'),
('øyvi_vike', 'Øyvind', 'Viken', '2004');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skierclubseason`
--

CREATE TABLE `skierclubseason` (
  `clubid` varchar(250) DEFAULT NULL,
  `skierName` varchar(250) NOT NULL,
  `seasonYear` int(11) NOT NULL,
  `totalDistance` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skierclubseason`
--

INSERT INTO `skierclubseason` (`clubid`, `skierName`, `seasonYear`, `totalDistance`) VALUES
('asker-ski', '?hal_?mos', 2016, 3),
(NULL, '?jan_tang', 2015, 2),
(NULL, '?jan_tang', 2016, 4),
('vindil', '?rut_?mos', 2016, 1237),
('skiklubben', '?rut_nord', 2016, 368),
('skiklubben', 'ande_andr', 2015, 23),
('skiklubben', 'ande_andr', 2016, 55),
('lhmr-ski', 'ande_rønn', 2015, 942),
('asker-ski', 'andr_stee', 2015, 440),
('asker-ski', 'andr_stee', 2016, 379),
('skiklubben', 'anna_næss', 2015, 3),
('skiklubben', 'anna_næss', 2016, 3),
('skiklubben', 'arne_anto', 2015, 32),
('skiklubben', 'arne_anto', 2016, 99),
('skiklubben', 'arne_inge', 2015, 1),
('skiklubben', 'arne_inge', 2016, 2),
('lhmr-ski', 'astr_amun', 2015, 961),
('lhmr-ski', 'astr_amun', 2016, 761),
('skiklubben', 'astr_sven', 2015, 1),
('skiklubben', 'astr_sven', 2016, 3),
('asker-ski', 'bent_håla', 2015, 19),
('skiklubben', 'bent_håla', 2016, 62),
('asker-ski', 'bent_svee', 2015, 125),
('asker-ski', 'beri_hans', 2015, 448),
('asker-ski', 'beri_hans', 2016, 374),
('asker-ski', 'bjør_aase', 2015, 121),
('asker-ski', 'bjør_aase', 2016, 116),
('asker-ski', 'bjør_ali', 2015, 47),
('asker-ski', 'bjør_ali', 2016, 47),
('lhmr-ski', 'bjør_rønn', 2015, 33),
('lhmr-ski', 'bjør_rønn', 2016, 56),
('lhmr-ski', 'bjør_sand', 2015, 460),
('lhmr-ski', 'bjør_sand', 2016, 449),
('skiklubben', 'bror_?mos', 2016, 243),
(NULL, 'bror_kals', 2016, 202),
('vindil', 'cami_erik', 2015, 1),
('vindil', 'cami_erik', 2016, 1),
('lhmr-ski', 'dani_hamm', 2015, 33),
('lhmr-ski', 'dani_hamm', 2016, 61),
('lhmr-ski', 'eina_nygå', 2015, 31),
('skiklubben', 'eina_nygå', 2016, 68),
('asker-ski', 'elis_ruud', 2015, 341),
('skiklubben', 'elis_ruud', 2016, 368),
('lhmr-ski', 'elle_wiik', 2015, 12),
('lhmr-ski', 'elle_wiik', 2016, 35),
('lhmr-ski', 'erik_haal', 2015, 122),
('lhmr-ski', 'erik_haal', 2016, 143),
('vindil', 'erik_lien', 2015, 1),
('vindil', 'erik_pete', 2015, 581),
('skiklubben', 'espe_egel', 2015, 519),
('skiklubben', 'espe_egel', 2016, 556),
('lhmr-ski', 'espe_haal', 2015, 1),
('lhmr-ski', 'espe_haal', 2016, 2),
('skiklubben', 'eva_kvam', 2015, 28),
('skiklubben', 'eva_kvam', 2016, 89),
('asker-ski', 'fred_lien', 2015, 113),
('asker-ski', 'fred_lien', 2016, 122),
('skiklubben', 'frod_mads', 2015, 1),
('skiklubben', 'frod_mads', 2016, 2),
('lhmr-ski', 'frod_rønn', 2015, 237),
('skiklubben', 'geir_birk', 2015, 69),
('skiklubben', 'geir_birk', 2016, 71),
('asker-ski', 'geir_herm', 2015, 891),
('skiklubben', 'geir_herm', 2016, 789),
('lhmr-ski', 'gerd_svee', 2015, 173),
('lhmr-ski', 'gerd_svee', 2016, 196),
('vindil', 'gunn_berg', 2015, 2),
('vindil', 'gunn_berg', 2016, 2),
('skiklubben', 'guri_nord', 2016, 17),
('lhmr-ski', 'hann_stei', 2016, 14),
('asker-ski', 'hans_foss', 2015, 240),
('lhmr-ski', 'hans_foss', 2016, 276),
('skiklubben', 'hans_løke', 2015, 3),
('skiklubben', 'hans_løke', 2016, 1),
('lhmr-ski', 'hara_bakk', 2015, 7),
('lhmr-ski', 'hara_bakk', 2016, 16),
('asker-ski', 'heid_dani', 2015, 3),
('asker-ski', 'heid_dani', 2016, 3),
('skiklubben', 'helg_brei', 2015, 27),
('skiklubben', 'helg_brei', 2016, 74),
('skiklubben', 'helg_toll', 2015, 9),
('asker-ski', 'henr_bern', 2015, 799),
(NULL, 'henr_dale', 2015, 2),
(NULL, 'henr_dale', 2016, 2),
('vindil', 'henr_lore', 2015, 1),
('vindil', 'henr_lore', 2016, 1),
('lhmr-ski', 'hild_hass', 2015, 2),
('lhmr-ski', 'hild_hass', 2016, 1),
('lhmr-ski', 'håko_jens', 2015, 778),
('lhmr-ski', 'håko_jens', 2016, 804),
('skiklubben', 'idar_kals', 2016, 101),
('vindil', 'idar_kals1', 2016, 1308),
('skiklubben', 'ida_mykl', 2015, 666),
('skiklubben', 'ida_mykl', 2016, 614),
('asker-ski', 'inge_simo', 2015, 3),
('asker-ski', 'inge_simo', 2016, 2),
(NULL, 'inge_thor', 2015, 194),
(NULL, 'inge_thor', 2016, 220),
('skiklubben', 'ingr_edva', 2015, 294),
('skiklubben', 'ingr_edva', 2016, 309),
('skiklubben', 'juli_ande', 2015, 20),
('skiklubben', 'juli_ande', 2016, 34),
('skiklubben', 'kari_thor', 2015, 261),
('skiklubben', 'kari_thor', 2016, 233),
('asker-ski', 'kjel_fjel', 2015, 1),
('skiklubben', 'kjel_fjel', 2016, 2),
('lhmr-ski', 'knut_bye', 2015, 2),
('skiklubben', 'kris_even', 2015, 586),
('skiklubben', 'kris_hass', 2015, 4),
('skiklubben', 'kris_hass', 2016, 11),
('skiklubben', 'kris_hass1', 2015, 391),
('lhmr-ski', 'kris_hass1', 2016, 334),
('lhmr-ski', 'lind_lore', 2015, 578),
('lhmr-ski', 'lind_lore', 2016, 551),
('asker-ski', 'liv_khan', 2015, 178),
('asker-ski', 'liv_khan', 2016, 183),
('asker-ski', 'magn_sand', 2015, 200),
('asker-ski', 'magn_sand', 2016, 166),
(NULL, 'mari_bye', 2015, 362),
('lhmr-ski', 'mari_dahl', 2015, 576),
('lhmr-ski', 'mari_dahl', 2016, 492),
('lhmr-ski', 'mari_eile', 2015, 18),
('lhmr-ski', 'mari_eile', 2016, 18),
('skiklubben', 'mari_stra', 2015, 41),
('skiklubben', 'mari_stra', 2016, 35),
('vindil', 'mart_halv', 2015, 63),
('vindil', 'mart_halv', 2016, 50),
('skiklubben', 'mona_lie', 2015, 7),
('skiklubben', 'mona_lie', 2016, 12),
('skiklubben', 'mort_iver', 2015, 2),
('skiklubben', 'mort_iver', 2016, 4),
('lhmr-ski', 'nils_bakk', 2015, 36),
('lhmr-ski', 'nils_bakk', 2016, 93),
('skiklubben', 'nils_knud', 2015, 4),
('skiklubben', 'nils_knud', 2016, 2),
('skiklubben', 'odd_moha', 2015, 352),
(NULL, 'olav_bråt', 2015, 17),
(NULL, 'olav_bråt', 2016, 19),
('lhmr-ski', 'olav_eike', 2015, 2),
('lhmr-ski', 'olav_eike', 2016, 2),
('skiklubben', 'olav_hell', 2015, 1),
('asker-ski', 'olav_lien', 2015, 408),
('asker-ski', 'olav_lien', 2016, 423),
('lhmr-ski', 'ole_borg', 2015, 311),
('lhmr-ski', 'ole_borg', 2016, 314),
('skiklubben', 'reid_hamr', 2015, 2),
('skiklubben', 'reid_hamr', 2016, 3),
('skiklubben', 'rolf_wiik', 2015, 749),
('skiklubben', 'rolf_wiik', 2016, 632),
('asker-ski', 'rune_haga', 2015, 228),
('asker-ski', 'rune_haga', 2016, 248),
('asker-ski', 'sara_okst', 2016, 5),
('asker-ski', 'silj_mykl', 2015, 1),
('asker-ski', 'silj_mykl', 2016, 2),
('skiklubben', 'sive_nord', 2016, 1),
(NULL, 'solv_solb', 2015, 2),
('asker-ski', 'solv_solb', 2016, 1),
(NULL, 'stia_andr', 2015, 8),
('vindil', 'stia_andr', 2016, 9),
('skiklubben', 'stia_haug', 2015, 412),
('skiklubben', 'stia_haug', 2016, 443),
('vindil', 'stia_henr', 2015, 62),
('vindil', 'stia_henr', 2016, 49),
('skiklubben', 'terj_mort', 2015, 119),
('skiklubben', 'terj_mort', 2016, 95),
('vindil', 'thom_inge', 2015, 15),
('vindil', 'thom_inge', 2016, 26),
('vindil', 'tom_bråt', 2015, 1),
('vindil', 'tom_bråt', 2016, 1),
('vindil', 'tom_bøe', 2015, 176),
('vindil', 'tom_bøe', 2016, 194),
('asker-ski', 'tom_jako', 2015, 18),
('skiklubben', 'tom_jako', 2016, 33),
('lhmr-ski', 'tore_gulb', 2015, 375),
('lhmr-ski', 'tore_gulb', 2016, 342),
('skiklubben', 'tore_svee', 2015, 1156),
('skiklubben', 'tor_dale', 2015, 408),
('asker-ski', 'tove_moe', 2015, 321),
('asker-ski', 'tove_moe', 2016, 352),
('lhmr-ski', 'trin_kals', 2016, 22),
('skiklubben', 'tron_kris', 2015, 3),
('skiklubben', 'tron_kris', 2016, 5),
('vindil', 'tron_moen', 2015, 8),
('vindil', 'tron_moen', 2016, 17),
('skiklubben', 'øyst_aase', 2015, 2),
('skiklubben', 'øyst_aase', 2016, 1),
('skiklubben', 'øyst_lore', 2015, 13),
('skiklubben', 'øyst_lore', 2016, 47),
('vindil', 'øyst_sæth', 2015, 831),
('vindil', 'øyst_sæth', 2016, 631),
('asker-ski', 'øyvi_hell', 2015, 950),
('asker-ski', 'øyvi_hell', 2016, 869),
('skiklubben', 'øyvi_jens', 2015, 3),
('skiklubben', 'øyvi_jens', 2016, 2),
('asker-ski', 'øyvi_kvam', 2015, 18),
('asker-ski', 'øyvi_vike', 2015, 20),
('asker-ski', 'øyvi_vike', 2016, 52);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `season`
--
ALTER TABLE `season`
  ADD PRIMARY KEY (`fallYear`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `skierclubseason`
--
ALTER TABLE `skierclubseason`
  ADD PRIMARY KEY (`skierName`,`seasonYear`),
  ADD KEY `clubid` (`clubid`),
  ADD KEY `seasonYear` (`seasonYear`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `skierclubseason`
--
ALTER TABLE `skierclubseason`
  ADD CONSTRAINT `skierclubseason_ibfk_1` FOREIGN KEY (`clubid`) REFERENCES `clubs` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `skierclubseason_ibfk_2` FOREIGN KEY (`skierName`) REFERENCES `skier` (`userName`) ON UPDATE CASCADE,
  ADD CONSTRAINT `skierclubseason_ibfk_3` FOREIGN KEY (`seasonYear`) REFERENCES `season` (`fallYear`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
